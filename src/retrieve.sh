#!/bin/bash
#Retrieve bash script created by Anna Sperotto, further modified by Rezfan Pawirotaroeno
#NOTES:
#this link contains also past blacklists http://mirror1.malwaredomains.com/files/
#more to check
# http://uribl.com/
# http://www.surbl.org/
#


#find out if I am runnig on mac os x or linux
unamestr=`uname`
if [ "$unamestr" == 'Linux' ]; then
 BASEDIR="/home/rezfan/prototype/proto/Data"
 printf "Running Linux\nLinux detected\n"
elif [ "$unamestr" == 'Darwin' ]; then
	#mac os x
	hname=`hostname`
	if [ "$hname" == 'nemo.lan' ]; then
		BASEDIR="/Users/name/Documents/_UT/code/malicious_domains_crawling"
	else
		BASEDIR="/Users/name/Documents/_VARIA/malicious_domain_crawling"
	fi
fi


LOG="$BASEDIR/logs/log.txt"
LOGERR="$BASEDIR/logs/err.txt"
DATADIR="$BASEDIR/data"

#crawl lists of malicous domain names
MALWAREDOMAINLIST_HOSTS='http://www.malwaredomainlist.com/hostslist/hosts.txt'
MALWAREDOMAINLIST_DELISTED='http://www.malwaredomainlist.com/hostslist/delisted.txt'
DNSBH='http://mirror1.malwaredomains.com/files/justdomains'
JOEWEIN='http://www.joewein.net/dl/bl/dom-bl.txt'
MALCODE='http://malc0de.com/bl/ZONES'
#ZEUSTRACKER='https://zeustracker.abuse.ch/blocklist.php?download=domainblocklist' Disconnected July 2019
#RANSOMWARETRACKER='https://ransomwaretracker.abuse.ch/downloads/RW_DOMBL.txt' Disconnected Dec 2019
#HOSTFILE='https://hosts-file.net/hphosts-partial.txt' #stopped on Feb 28, 2020 - not updated anymore
#paleve discontinued since Tue Dec  6 2016
#PALEVO='https://palevotracker.abuse.ch/blocklists.php?download=domainblocklist'
#feodo does not track domains anymore (discovered Dec 30, 2018)
#FEODOTRACKER='https://feodotracker.abuse.ch/blocklist/?download=domainblocklist'
#HPHOSTS='https://hosts-file.net/download/hosts.txt' #stopped on Feb 28, 2020 - not updated anymore
#THREATEXPERT='http://www.networksec.org/grabbho/block.txt' #see https://isc.sans.edu/suspicious_domains.html
OPENPHISH='https://openphish.com/feed.txt'
THREATCROWD='https://www.threatcrowd.org/feeds/domains.txt'
#URLHAUS='https://urlhaus.abuse.ch/downloads/text/'
URLHAUS='https://urlhaus.abuse.ch/downloads/hostfile/'
C2DOM='http://osint.bambenekconsulting.com/feeds/c2-dommasterlist.txt'
VXVAULT='http://vxvault.net/URL_List.php'
CYBERCRIMETRACKER='http://cybercrime-tracker.net/all.php'
#SQUIDBLACKLISTMALICIOUS="https://www.squidblacklist.org/downloads/dg-malicious.acl" #offline in late 2019? Maintainer is dead :(
DSHIELD='https://dshield.org/feeds/suspiciousdomains_Medium.txt'
PONMOCUP='http://security-research.dyndns.org/pub/malware-feeds/ponmocup-infected-domains-shadowserver.csv'
URLVIR='http://www.urlvir.com/export-hosts'
TOLOUSECRYPTO='http://dsi.ut-capitole.fr/blacklists/download/cryptojacking.tar.gz'
TOLOUSEDDOS='http://dsi.ut-capitole.fr/blacklists/download/ddos.tar.gz'
TOLOUSEMALWARE='http://dsi.ut-capitole.fr/blacklists/download/malware.tar.gz'
TOLOUSEPHISHING='http://dsi.ut-capitole.fr/blacklists/download/phishing.tar.gz'
COINBLOCKER='https://zerodot1.gitlab.io/CoinBlockerLists/hosts_browser'
HEXXIUM='https://raw.githubusercontent.com/HexxiumCreations/threat-list/gh-pages/hexxiumthreatlist.txt'
ETHEREUMPHISHING='https://api.infura.io/v2/blacklist'
DIGITALSIDE='https://osint.digitalside.it/Threat-Intel/lists/latestdomains.txt'
PHISHINGARMY='https://phishing.army/download/phishing_army_blocklist_extended.txt' #note this is Openphish, Phishtank and PhishFindR
QUIDSUP='https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-malware.txt'
HORUSTEKNOLOJI='https://raw.githubusercontent.com/HorusTeknoloji/TR-PhishingList/master/url-lists.txt'
TECHNOY='https://www.technoy.de/lists/malware2.txt'
RPILIST='https://raw.githubusercontent.com/RPiList/specials/master/Blocklisten/malware'
BLPROJECT_MAL='https://raw.githubusercontent.com/blocklistproject/Lists/master/malware.txt'
BLPROJECT_RAN='https://raw.githubusercontent.com/blocklistproject/Lists/master/ransomware.txt'
RPILIST_CRYPTO='https://raw.githubusercontent.com/RPiList/specials/master/Blocklisten/crypto'

names[1]="malwaredomainlist_hosts" #deprecated
names[2]="malwaredomainlist_delisted" #should be removed
names[3]="joewein" #
names[4]="malcode" #
names[5]="openphish" #
names[6]="dnsbh" #removed
names[7]="urlhaus" #
names[8]="c2dom" #removed?
names[9]="vxvault" #
names[10]="cybercrimetracker" #
names[11]='dshield' #removed
names[12]='ponmocup' #
names[13]='urlvir' #removed
names[14]='tolousecrypto' #
names[15]='tolouseddos' #
names[16]='tolousemalware' #
names[17]='tolousephishing' #
names[18]='coinblocker' #
names[19]='hexxium' #
names[20]='ethereumphishing'  #removed
names[21]='digitalside' #
names[22]='phishingarmy' #
names[23]='quidsup' #
names[24]='horusteknoloji' #
names[25]='technoy' #
names[26]='rpilist' #
names[27]='blproject_mal' #
names[28]='blproject_ran' #
names[29]='rpilist_crypto' #




LISTS="$MALWAREDOMAINLIST_HOSTS $MALWAREDOMAINLIST_DELISTED $JOEWEIN $MALCODE $OPENPHISH $DNSBH $URLHAUS $C2DOM $VXVAULT $CYBERCRIMETRACKER $DSHIELD $PONMOCUP $URLVIR $TOLOUSECRYPTO $TOLOUSEDDOS $TOLOUSEMALWARE $TOLOUSEPHISHING $COINBLOCKER $HEXXIUM $ETHEREUMPHISHING $DIGITALSIDE $PHISHINGARMY $QUIDSUP $HORUSTEKNOLOJI $TECHNOY $RPILIST $BLPROJECT_MAL $BLPROJECT_RAN $RPILIST_CRYPTO"


today=`date +%Y-%m-%d`
counter=1

echo "`date`      Crawling..." >>$LOG

for LIST in $LISTS
do

	#check if tar.gz (needed for U Tolouse lists)
	ending=`expr "$LIST" : '.*\(...\...\)'`
        pattern="tar.gz"

	if [ "$ending" == "$pattern" ]; then

		curl -s -f -o "/tmp/mal_dom_crawling_tmp.tar.gz" $LIST
		mkdir /tmp/mal
		tar xzf /tmp/mal_dom_crawling_tmp.tar.gz -C /tmp/mal/
		mv /tmp/mal/*/domains $DATADIR/${today}_${names[$counter]}
		rm /tmp/mal_dom_crawling_tmp.tar.gz
		rm -r /tmp/mal
	else
    	curl -s -f -o $DATADIR/${today}_${names[$counter]} $LIST
    	if [ $? -ne 0 ]; then
			echo "`date`   -curl failed when retrieving $LIST" >>$LOGERR
			echo "`date`         ${names[$counter]} FAIL" >>$LOG
    	else
			echo "`date`         ${names[$counter]} OK" >>$LOG
    	fi
    fi

    counter=`expr $counter + 1`
done


#merge the blacklists for today (take care of virtualenv on linux machine)
#
# todayname = ""
# todayname ${today}
# todayname = ech todayname | sed 's/_//g'
if [ "$unamestr" == 'Linux' ]; then
	#debian
	python2 $BASEDIR/venv/bin/parse.py -d $today $BASEDIR/data/ > /$BASEDIR/merged/${today}.txt
	# $BASEDIR/venv/bin/python $BASEDIR/scripts/parse.py -d $today $BASEDIR/data/ > /$BASEDIR/merged/${today}
elif [ "$unamestr" == 'Darwin' ]; then
	#mac os x
	python $BASEDIR/scripts/parse.py -d $today $BASEDIR/data/ > /$BASEDIR/merged/${today}
fi
