# SURF DNS Internship Project
## _Privacy-Aware Blocklist Evaluator tool_


[![N|Solid](https://www.surf.nl/themes/surf/logo.svg)](https://www.surf.nl/)

This tool is developed with the goal of evaluating the effectiveness of blocklists in an active DNS resolver while staying privacy-aware. This readme explains the general program in case further adjustments would like to be made. In case of additional questions, feel free to contact me at r.z.g.pawirotaroeno@student.utwente.nl. 

This flow chart explains the general flow of the program (more detail found in report):
[![N|Solid](https://i.ibb.co/rw0vMW8/flowchart1.png)](https://www.surf.nl/)

## Features

- Gathering lists of malicious domains from around 23 different blocklist providers
- Checking if the entries of the blocklists are present in stored bloomfilters 
- Generating data + different graphs for quick scanning & further analysis

## Tech

The tool consists of different programs:

- [Python3] - Used in run.py & reset.py
- [Python2] - used in parse.py (tldextract)
- [Golang] - Used in lookup module
- [Bash] - Used in retrieve.sh 
- [Unbound] - System running dnstap, which communicates with bloomfilter

## Components
- **run.py**: Main python script that will start filling up the different "step_*" directories. Requires input as input files: blocklists + bloomfilters. These can be placed in the "step_0-INPUT" folder or the input directories for the blocklists and bloomfilters can be changed in the code itself for convenience. 
- **reset.py**: Main goal for troubleshooting and testing. Running this file will remove all files from the "step_*" directories and fill the default input folder with sample blocklists and bloomfilters taken from "backup folder."
- **"backup"** folder: Contains example blocklists and bloomfilters to use for prototyping.
- **"src"** folder: Contains retrieve.sh, which is used for gathering and merging malicious domains from the internet, and generating blocklists that can be used as input for run.py or step_0. Also contains **lookup** binary, which is compiled in go and is called in run.py. 
- **"Data"** folder: /src/retrieve.sh saves its raw data + merged blocklists in this directory. Furthermore also the "~/Data/venv/bin/" contains the **parse.py** script, which is used by the **retrieve.sh** script in order to parse the information gathered by the different providers.
- **"step_0-INPUT"** folder: Directory in which input bloomfilter + blocklists can be specified. Furthermore also contains a log for which bloomfilters and blocklists have already been checked (and thus do not need to be checked again). 
- **"step_1-filtered_blocklist"** folder: This folder contains the malicious domains from the merged blocklists (so does not contain information regarding the providers of the entries), which will be used by the lookup binary to return the blocklist hits. 
- **"step_2-bloomfilter_hits"** folder: The entries that were specified in the blocklists and have been found in the bloomfilters will be returned to this folder. 
- **"step_3-data"** folder: This folder contains the name of the malicious domain that was marked as a hit, along with the blocklist providers that mentioned this domain as a malicious one in the merged blocklist from (either step_0/blocklist or /Data/merged)
- **"step_4-results"** folder: This folder contains information regarding the amount of hits for each provider in .xlsx format. Which can be converted to a dataframe for further processing. 
- **"step_5-statistics"** folder: This folder contains a graph for the distribution of the hits between the providers. Can be quickly scanned through to observe occurrencies. 


## Installation 
Program made to run and testen on Debian based systems. Most of the prerequisites come with Debian based systems by default. 

Installing pandas:
```sh
pip install pandas
```

Installing tldextract for python2 used in parse.py: 
```sh
sudo apt install curl
curl https://bootstrap.pypa.io/pip/2.7/get-pip.py --output get-pip.py
sudo python2 get-pip.py
pip2 –version
pip2 install tldextract
```

## Running the program
- Ensure file is downloaded, bloomfilters are set up to be stored in the bloomfilters directory in the input file. 
- run.py can be run and will take all input files to generate the output files, can also be added to crontab to run periodically. 
- Run run.py using python3 and as input give the directory in which the run.py script is located. 

```sh
sudo python3 run.py [directory]
```

- Adjust ~/Data/src/retrieve.sh by setting "BASEDIR" string to the directory containing ~/Data folder.

## Additional Notes
- Script itelf contains comments for more clarification
- This readme does not contain the setup for the bloomfilter and unbound, however these are also important. An unbound server config needs to be adjusted in order to communicate with a bloomfilter module from which bloomfilter (binaries) will be generated. For more info regarding this, please contact joeri.deruiter@surf.nl. 
- The names of the files are used to determine their date and time, so a change in any of the filenames (blocklists/bloomfilters) will result in several errors. However, adjusting the input strings in (run.py) can be done in order to solve this issue. 




[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [Python3]: <https://github.com/joemccann/dillinger>
   [Python2]: <https://github.com/joemccann/dillinger.git>
   [Golang]: <http://daringfireball.net>
   [Bash]: <http://daringfireball.net/projects/markdown/>
   [Unbound]: <http://daringfireball.net/projects/markdown/>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>
