#!/usr/bin/env python

# This script performs the following tasks
# 1) for each blacklist of malicious domain names retrieved, it offers a
# parsing function that has the goal of rewriting the domain name
# 2) it merges all domain blacklists collected in the same day and output a single list of
# malicious domain names

from optparse import OptionParser
import sys, os
import gzip
import subprocess
import glob
import re
import time
import pickle
import random
import itertools
import tldextract
import subprocess
import idna
import json




#*******************************************************************************
# usage
#*******************************************************************************
def process_opt():
  usage = "usage: %prog [options] <domain blacklist dir> "
  parser = OptionParser(usage=usage)
  parser.add_option("-d", "", dest="date", default="",
                    help="Which day do you want to parse and merge? (format: YYYY-MM-DD)")
  opt, directories = parser.parse_args()

  if (len(directories) != 1):
    parser.print_help()
    sys.exit(1)
  return opt, directories


#*******************************************************************************
# idna_encoding
#*******************************************************************************
def idna_econding(name):
	encoded_name = unicode("")
	try:
		encoded_name = idna.encode(name)
	except Exception:
		try:
			encoded_name = idna.encode(name.decode('utf-8'))
		except Exception:
			try:
				encoded_name = idna.encode(name.decode('latin1'))
			except Exception:
				try:
					encoded_name = idna.encode(name.decode('gb2312'))
				except Exception:
					pass
                    #sys.stderr.write("Giving up on name " + name + "\n")
	return encoded_name


#*******************************************************************************
# parse_joewein
#*******************************************************************************
def parse_joewein(input_dir, d):
    fname = input_dir + d + "_joewein"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            parts = line.split(";")
            domain = parts[0].rstrip()

            ext = tldextract.extract(domain)
            sld = ext[1] + "." + ext[2]

            if not (sld in domains_dic):
                domains_dic[sld] = ['joewein', d, d]
    return domains_dic


#*******************************************************************************
# parse_malcode
#*******************************************************************************
def parse_malcode(input_dir, d):
    fname = input_dir + d + "_malcode"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            if line.startswith("zone"):
                parts = line.split(" ")
                domain = parts[1].lstrip('"').rstrip('"').rstrip()

                ext = tldextract.extract(domain)
                sld = ext[1] + "." + ext[2]

                if not (sld in domains_dic):
                    domains_dic[sld] = ['malcode', d, d]
    return domains_dic

#*******************************************************************************
# parse_zeustracker
#*******************************************************************************
def parse_zeustracker(input_dir, d):
    fname = input_dir + d + "_zeustracker"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            if not line.startswith("#") or not line.rstrip():
                domain = line.rstrip()

                ext = tldextract.extract(domain)
                sld = ext[1] + "." + ext[2]

                if not (sld in domains_dic):
                    domains_dic[sld] = ['zeustracker', d, d]
    return domains_dic


#*******************************************************************************
# parse_ransomwaretracker
#*******************************************************************************
def parse_ransomwaretracker(input_dir, d):
    fname = input_dir + d + "_ransomwaretracker"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            if not line.startswith("#") or not line.rstrip():
                domain = line.rstrip()

                ext = tldextract.extract(domain)
                sld = ext[1] + "." + ext[2]

                if not (sld in domains_dic):
                    domains_dic[sld] = ['ransomwaretracker', d, d]
    return domains_dic

#*******************************************************************************
# parse_hostfile
#*******************************************************************************
def parse_hostfile(input_dir, d):
    fname = input_dir + d + "_hostfile"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            if line.startswith("127"):
                parts = line.split("\t")
                domain = parts[1]

                ext = tldextract.extract(domain)
                sld = ext[1] + "." + ext[2]

                if not (sld in domains_dic):
                    domains_dic[sld] = ['hostfile', d, d]
    return domains_dic


#*******************************************************************************
# parse_hphosts
#*******************************************************************************
def parse_hphosts(input_dir, d):
    fname = input_dir + d + "_hphosts"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            if line.startswith("127"):
                parts = line.split("\t")
                domain = parts[1]

                ext = tldextract.extract(domain)
                sld = ext[1] + "." + ext[2]

                if not (sld in domains_dic):
                    domains_dic[sld] = ['hphosts', d, d]
    return domains_dic

#*******************************************************************************
# parse_threatexpert
#*******************************************************************************
def parse_threatexpert(input_dir, d):
    fname = input_dir + d + "_threatexpert"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            if not line.startswith("#") or not line.rstrip():
                domain = line.rstrip()

                ext = tldextract.extract(domain)
                sld = ext[1] + "." + ext[2]

                if not (sld in domains_dic):
                    domains_dic[sld] = ['threatexpert', d, d]
    return domains_dic

#*******************************************************************************
# parse_openphish
#*******************************************************************************
def parse_openphish(input_dir, d):
    fname = input_dir + d + "_openphish"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            url = line.rstrip()

            ext = tldextract.extract(url)
            sld = ext[1] + "." + ext[2]

            if not (sld in domains_dic):
                domains_dic[sld] = ['openphish', d, d]
    return domains_dic

#*******************************************************************************
# parse_malwaredomainlist
#*******************************************************************************
def parse_malwaredomainlist(input_dir, d):
    fname = input_dir + d + "_malwaredomainlist_delisted"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            if line.startswith("127"):
                parts = line.split(" ")
                domain = parts[2]

                ext = tldextract.extract(domain.rstrip())
                sld = ext[1] + "." + ext[2]

                if not (sld in domains_dic):
                    domains_dic[sld] = ['malwaredomainlist', d, d]
    return domains_dic


#*******************************************************************************
# parse_dnsbh
#*******************************************************************************
def parse_dnsbh(input_dir, d):
    fname = input_dir + d + "_dnsbh"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            parts = line.split(";")
            domain = parts[0].rstrip()

            ext = tldextract.extract(domain)
            sld = ext[1] + "." + ext[2]

            if not (sld in domains_dic):
                domains_dic[sld] = ['dnsbh', d, d]
    return domains_dic


#*******************************************************************************
# parse_urlhaus
#*******************************************************************************
def parse_urlhaus(input_dir, d):
    fname = input_dir + d + "_urlhaus"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            if not line.startswith("#") or not line.rstrip():
                domain = line.rstrip()

                ext = tldextract.extract(domain)
                sld = ext[1] + "." + ext[2]

                if not (sld in domains_dic):
                    domains_dic[sld] = ['urlhaus', d, d]
    return domains_dic



#*******************************************************************************
# parse_c2dom
#*******************************************************************************
def parse_c2dom(input_dir, d):
    fname = input_dir + d + "_c2dom"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            if not line.startswith("#") or not line.rstrip():
                raw = line.rstrip()
                parts = raw.split(',')
                domain = parts[0]

                ext = tldextract.extract(domain)
                sld = ext[1] + "." + ext[2]

                if not (sld in domains_dic):
                    domains_dic[sld] = ['c2dom', d, d]
    return domains_dic

#*******************************************************************************
# parse_vxvault
#*******************************************************************************
def parse_vxvault(input_dir, d):
    fname = input_dir + d + "_vxvault"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            if line.startswith("http"):
                domain = line.rstrip()

                ext = tldextract.extract(domain)
                sld = ext[1] + "." + ext[2]

                if not (sld in domains_dic):
                    domains_dic[sld] = ['vxvault', d, d]
    return domains_dic

#*******************************************************************************
# parse_cybercrimetracker
#*******************************************************************************
def parse_cybercrimetracker(input_dir, d):
    fname = input_dir + d + "_cybercrimetracker"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            domain = line.rstrip()

            ext = tldextract.extract(domain)
            sld = ext[1] + "." + ext[2]

            if not (sld in domains_dic):
                domains_dic[sld] = ['cybercrimetracker', d, d]
    return domains_dic


#*******************************************************************************
# parse_squidblacklistmalicious
#*******************************************************************************
def parse_squidblacklistmalicious(input_dir, d):
    fname = input_dir + d + "_squidblacklistmalicious"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            if not line.startswith("#") or not line.rstrip():
                domain = line.rstrip()

                ext = tldextract.extract(domain)
                sld = ext[1] + "." + ext[2]

                if not (sld in domains_dic):
                    domains_dic[sld] = ['squidblacklistmalicious', d, d]
    return domains_dic

#*******************************************************************************
# parse_dshield
#*******************************************************************************
def parse_dshield(input_dir, d):
    fname = input_dir + d + "_dshield"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            if not line.startswith("#") or not line.rstrip():
                domain = line.rstrip()

                ext = tldextract.extract(domain)
                sld = ext[1] + "." + ext[2]

                if not (sld in domains_dic):
                    domains_dic[sld] = ['dshield', d, d]
    return domains_dic

#*******************************************************************************
# parse_ponmocup
#*******************************************************************************
def parse_ponmocup(input_dir, d):
    fname = input_dir + d + "_ponmocup"
    domains_dic = {}

    with open(fname) as f:
        for line in f:

            if not line.startswith("timestamp") or not line.rstrip():
                parts = line.split(",")

                domain = parts[2].strip("\"").rstrip()
                domain2 = parts[7].strip("\"").rstrip()

                ext = tldextract.extract(domain)
                ext2 = tldextract.extract(domain2)


                sld = ext[1] + "." + ext[2]
                sld2 = ext2[1] + "." + ext2[2]

                if not (sld in domains_dic):
                    domains_dic[sld] = ['ponmocup', d, d]
                if not (sld2 in domains_dic):
                    domains_dic[sld2] = ['ponmocup', d, d]
    return domains_dic

#*******************************************************************************
# parse_urlvir
#*******************************************************************************
def parse_urlvir(input_dir, d):
    fname = input_dir + d + "_urlvir"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            if not line.startswith("#") or not line.rstrip():
                domain = line.rstrip()

                ext = tldextract.extract(domain)
                sld = ext[1] + "." + ext[2]

                if not (sld in domains_dic):
                    domains_dic[sld] = ['urlvir', d, d]
    return domains_dic


#*******************************************************************************
# parse_tolousecrypto
#*******************************************************************************
def parse_tolousecrypto(input_dir, d):
    fname = input_dir + d + "_tolousecrypto"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            domain = line.rstrip()

            ext = tldextract.extract(domain)

            sld = ext[1] + "." + ext[2]

            if not (sld in domains_dic) and '0.0.0.0' not in domain:
                domains_dic[sld] = ['tolousecrypto', d, d]
    return domains_dic

#*******************************************************************************
# parse_tolouseddos
#*******************************************************************************
def parse_tolouseddos(input_dir, d):
    fname = input_dir + d + "_tolouseddos"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            domain = line.rstrip()

            ext = tldextract.extract(domain)
            sld = ext[1] + "." + ext[2]

            if not (sld in domains_dic):
                domains_dic[sld] = ['tolouseddos', d, d]
    return domains_dic


#*******************************************************************************
# parse_tolousemalware
#*******************************************************************************
def parse_tolousemalware(input_dir, d):
    fname = input_dir + d + "_tolousemalware"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            domain = line.rstrip()

            ext = tldextract.extract(domain)
            sld = ext[1] + "." + ext[2]

            if not (sld in domains_dic):
                domains_dic[sld] = ['tolousemalware', d, d]
    return domains_dic

#*******************************************************************************
# parse_tolousephishing
#*******************************************************************************
def parse_tolousephishing(input_dir, d):
    fname = input_dir + d + "_tolousephishing"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            domain = line.rstrip()

            ext = tldextract.extract(domain)
            sld = ext[1] + "." + ext[2]

            if not (sld in domains_dic):
                domains_dic[sld] = ['tolousephishing', d, d]
    return domains_dic

#*******************************************************************************
# parse_coinblocker
#*******************************************************************************
def parse_coinblocker(input_dir, d):
    fname = input_dir + d + "_coinblocker"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            if not line.startswith("#") or not line.rstrip():
                parts = line.split(" ")
                domain = parts[1].rstrip()
                #print('domain: ', domain)

                ext = tldextract.extract(domain)
                #print('extension: ', ext)
                sld = ext[1] + "." + ext[2]
                #print('second level domain: ', sld)

                if not (sld in domains_dic):
                    domains_dic[sld] = ['coinblocker', d, d]
    return domains_dic



#*******************************************************************************
# parse_hexxium
#*******************************************************************************
def parse_hexxium(input_dir, d):
    fname = input_dir + d + "_hexxium"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            line = line.rstrip()
            if not line.startswith("!") or not line.rstrip():
                parts = line.split("|")
                domain = parts[2].rstrip().rstrip('^').rstrip()

                try:
                    ext = tldextract.extract(domain)
                    sld = ext[1] + "." + ext[2]
                    if not (sld in domains_dic):
                        domains_dic[sld] = ['hexxium', d, d]
                except: #tldextract may rise an exception
                    pass
    return domains_dic

#*******************************************************************************
# parse_ethereumphishing
#*******************************************************************************
def parse_ethereumphishing(input_dir, d):
    fname = input_dir + d + "_ethereumphishing"
    domains_dic = {}

    with open(fname) as f:
        data = json.load(f)
        for domain in data["blacklist"]:
            ext = tldextract.extract(domain)
            sld = ext[1] + "." + ext[2]
            if not (sld in domains_dic):
                domains_dic[sld] = ['ethereumphishing', d, d]
    return domains_dic


#*******************************************************************************
# parse_digitalside
#*******************************************************************************
def parse_digitalside(input_dir, d):
    fname = input_dir + d + "_digitalside"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            if not line.startswith("#") or not line.rstrip():
                domain = line.rstrip()

                ext = tldextract.extract(domain)
                sld = ext[1] + "." + ext[2]

                if not (sld in domains_dic):
                    domains_dic[sld] = ['digitalside', d, d]
    return domains_dic


#*******************************************************************************
# parse_phisingarmy
#*******************************************************************************
def parse_phisingarmy(input_dir, d):
    fname = input_dir + d + "_phishingarmy"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            if not line.startswith("#") or not line.rstrip():
                domain = line.rstrip()

                ext = tldextract.extract(domain)
                sld = ext[1] + "." + ext[2]

                if not (sld in domains_dic):
                    domains_dic[sld] = ['phishingarmy', d, d]
    return domains_dic


#*******************************************************************************
# parse_quidsup
#*******************************************************************************
def parse_quidsup(input_dir, d):
    fname = input_dir + d + "_quidsup"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            if not line.startswith("#") or not line.rstrip():
                parts = line.split(" ")
                domain = parts[0]

                ext = tldextract.extract(domain)
                sld = ext[1] + "." + ext[2]

                if not (sld in domains_dic):
                    domains_dic[sld] = ['quidsup', d, d]
    return domains_dic

#*******************************************************************************
# parse_horusteknoloji
#*******************************************************************************
def parse_horusteknoloji(input_dir, d):
    fname = input_dir + d + "_horusteknoloji"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            domain = line.rstrip()
            ext = tldextract.extract(domain)
            sld = ext[1] + "." + ext[2]
            if not (sld in domains_dic):
                domains_dic[sld] = ['horusteknoloji', d, d]

    return domains_dic

#*******************************************************************************
# parse_dbl
#*******************************************************************************
def parse_dbl(input_dir, d):
    fname = input_dir + d + "_dbl"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            if not line.startswith("#") or not line.rstrip():
                if not  re.match("(\.[0-9]*$)", line):
                    domain = line.rstrip()
                    ext = tldextract.extract(domain)
                    sld = ext[1] + "." + ext[2]
                    if not sld == "." :
                        if not (sld in domains_dic):
                            domains_dic[sld] = ['dbl', d, d]

    return domains_dic


#*******************************************************************************
# merge_blacklists
#*******************************************************************************\
# DICTIONARY format
# |domain|source|first seen|last seen|
def merge_blacklists(bl_dic_1, bl_dic_2):
    merged_dic = {}
    for domain in bl_dic_1:
        if domain in bl_dic_2:
            sources = bl_dic_1[domain][0] + " " + bl_dic_2[domain][0]
            d1 = min(bl_dic_1[domain][1],bl_dic_2[domain][1])
            d2 = max(bl_dic_1[domain][2],bl_dic_2[domain][2])
            merged_dic[domain] = [sources, d1, d2]
        else :
            merged_dic[domain] = bl_dic_1[domain]

    for domain in bl_dic_2:
        if not (domain in merged_dic) :
            merged_dic[domain] = bl_dic_2[domain]

    return merged_dic

#*******************************************************************************
# parse_technoy
#*******************************************************************************
def parse_technoy(input_dir, d):
    fname = input_dir + d + "_technoy"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            parts = line.split(";")
            domain = parts[0].rstrip()

            ext = tldextract.extract(domain)
            sld = ext[1] + "." + ext[2]

            if not (sld in domains_dic):
                domains_dic[sld] = ['technoy', d, d]
    return domains_dic

#*******************************************************************************
# parse_rpilist
#*******************************************************************************
def parse_rpilist(input_dir, d):
    fname = input_dir + d + "_rpilist"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            parts = line.split(";")
            domain = parts[0].rstrip()

            ext = tldextract.extract(domain)
            sld = ext[1] + "." + ext[2]

            if not (sld in domains_dic):
                domains_dic[sld] = ['rpilist', d, d]
    return domains_dic

#*******************************************************************************
# parse_blproject_mal
#*******************************************************************************
def parse_blproject_mal(input_dir, d):
    fname = input_dir + d + "_blproject_mal"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            parts = line.split(";")
            domain = parts[0].rstrip()

            ext = tldextract.extract(domain)
            sld = ext[1] + "." + ext[2]

            if not (sld in domains_dic):
                domains_dic[sld] = ['blproject_mal', d, d]
    return domains_dic


#*******************************************************************************
# parse_blproject_ran
#*******************************************************************************
def parse_blproject_ran(input_dir, d):
    fname = input_dir + d + "_blproject_ran"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            parts = line.split(";")
            domain = parts[0].rstrip()

            ext = tldextract.extract(domain)
            sld = ext[1] + "." + ext[2]

            if not (sld in domains_dic):
                domains_dic[sld] = ['blproject_ran', d, d]
    return domains_dic

#*******************************************************************************
# parse_rpilist_crypto
#*******************************************************************************
def parse_rpilist_crypto(input_dir, d):
    fname = input_dir + d + "_rpilist_crypto"
    domains_dic = {}

    with open(fname) as f:
        for line in f:
            parts = line.split(";")
            domain = parts[0].rstrip()

            ext = tldextract.extract(domain)
            sld = ext[1] + "." + ext[2]

            if not (sld in domains_dic):
                domains_dic[sld] = ['rpilist_crypto', d, d]
    return domains_dic

#*******************************************************************************
# Main
#*******************************************************************************
if __name__ == '__main__':

    #list of malicious domain blacklists
    blacklists = ["malwaredomainlist_hosts", "joewein", "malcode", "openphish", "dnsbh", "urlhaus", "c2dom", "vxvault", "cybercrimetracker", "dshield", "ponmocup", "urlvir"]
    blacklists.append("tolousecrypto")
    blacklists.append("tolouseddos")
    blacklists.append("tolousemalware")
    blacklists.append("tolousephishing")
    blacklists.append("coinblocker")
    blacklists.append("hexxium")
    blacklists.append("ethereumphishing")
    blacklists.append("digitalside")
    blacklists.append("phishingarmy")
    blacklists.append("quidsup")
    blacklists.append("horusteknoloji")
    blacklists.append("dbl")
    blacklists.append("technoy")
    blacklists.append("rpilist")
    blacklists.append("blproject_mal")
    blacklists.append("blproject_ran")
    blacklists.append("rpilist_crypto")
    functions = {0 : parse_malwaredomainlist,
                 1 : parse_joewein,
                 2 : parse_malcode,
                 3 : parse_openphish,
                 4 : parse_dnsbh,
                 5 : parse_urlhaus,
                 6 : parse_c2dom,
                 7 : parse_vxvault,
                 8 : parse_cybercrimetracker,
                 9 : parse_dshield,
                 10 : parse_ponmocup,
                 11 : parse_urlvir,
                 12 : parse_tolousecrypto,
                 13 : parse_tolouseddos,
                 14 : parse_tolousemalware,
                 15 : parse_tolousephishing,
                 16 : parse_coinblocker,
                 17 : parse_hexxium,
                 18 : parse_ethereumphishing,
                 19 : parse_digitalside,
                 20 : parse_phisingarmy,
                 21 : parse_quidsup,
                 22 : parse_horusteknoloji,
                 23 : parse_dbl,
                 24 : parse_technoy,
                 25 : parse_rpilist,
                 26 : parse_blproject_mal,
                 27 : parse_blproject_ran,
                 28 : parse_rpilist_crypto
    }

    merged_dic = {}

    # parse input
    opt, directories = process_opt()

    #check if the input and output dir exists
    if (not os.path.isdir(directories[0])):
        sys.stderr.write("No input directory found!\n")
        sys.exit()

    #extract directories
    input_dir = directories[0]
    print(input_dir)
    #extract date
    d = opt.date


    for i in range(0, len(blacklists)):
        #check if a relevant file exist for the considered date, if yes, parse
        bl = blacklists[i]
        sys.stderr.write("Processing:" + input_dir + d +  "_" + bl + "\n")
        if (os.path.isfile(input_dir + d + "_" + bl)) :
            current_dic = functions[i](input_dir, d)
            merged_dic = merge_blacklists(merged_dic, current_dic)
        else :
            sys.stderr.write("\t" + input_dir + d +  "_" + bl + ": This file does not exists\n")

    for key in merged_dic :
        #print key
        name_encoded = idna_econding(key)
        if not len(name_encoded) == 0 :
            print name_encoded + "|" + merged_dic[key][0] + "|" + merged_dic[key][1]
