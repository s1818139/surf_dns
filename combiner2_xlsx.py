#This script is made to combine the different .xlsx into one file for an overview
#only change mydir and savedir to paths input and output files

import os
import sys
import argparse
import glob
from time import sleep
import pandas as pd
import matplotlib.pyplot as plt

#Change mydir to the directory containing the .xlsx files you want to combine (make sure formatting is same as .xlsx generated from run.py)
mydir = '/home/kali/Documents/transfer1/'

#Change savedir to the directory you want the output to be
savedir = '/home/kali/Documents/transfered1/'
all_files_in_2 = os.listdir(mydir)
xlsx_files = filter(lambda x: x[-5:] == '.xlsx', all_files_in_2)
mylist = list(xlsx_files)

#print(mylist[0])
myinput = mylist[0]


providerList = []
providerList.append('coinblocker')
providerList.append('cybercrimetracker')
providerList.append('dbl')
providerList.append('digitalside')
providerList.append('hexxium')
providerList.append('joewein')
providerList.append('malcode')
#providerList.append('malwaredomainlist') #need to be removed
providerList.append('openphish')
providerList.append('phishingarmy')
providerList.append('ponmocup')
providerList.append('quidsup')
providerList.append('tolousecrypto')
providerList.append('tolouseddos')
providerList.append('tolousemalware')
providerList.append('tolousephishing')
providerList.append('urlhaus')
providerList.append('vxvault')
providerList.append('technoy')
providerList.append('rpilist')
providerList.append('blproject_mal')
providerList.append('blproject_ran')
providerList.append('rpilist_crypto' )
providerList.append('horusteknoloji' )


dataStart = pd.DataFrame({'Providers': providerList})

def xlsxCombine(input):
    global dataStart
    df = pd.read_excel(mydir + input, index_col= False)
    name = input[-7:]
    hour = name[:-5]
    df2 = df.iloc[:,[1,2]]
    colList = list(df2.columns)
    colname = colList[1]
    df4 = df2.rename(columns={colname:colname + ' '+ hour + ':00:00'})
    day = df2[colList[1]]

    df3 = df4.iloc[:, 1]
    output = dataStart.join(df3)
    dataStart = output


for element in mylist:
    xlsxCombine(element)

dataStart_T = dataStart.T

#Normal compiled excel overview:
dataStart.to_excel(savedir+'output.xlsx')

#Transposed compiled excel overview:
dataStart_T.to_excel(savedir+'output_T.xlsx')
print('Completed')
