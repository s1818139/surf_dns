#Script made by Rezfan Pawirotaroeno for internship from University of Twente at SURF.
#It combines information from blocklists and bloomfilters and returns hits in .xlsx and .png
#Fully working prototype on SURF server @145.220.76.231

import os
import sys
import argparse
import glob
from time import sleep
import pandas as pd
import matplotlib.pyplot as plt

def print_paths(folder):

    #Defining folder paths
    path0 = os.path.join(folder,  'step_0-INPUT/')
    path1 = os.path.join(folder,  'step_1-filtered_blocklist/')
    path2 = os.path.join(folder,  'step_2-bloomfilter_hits/')
    path3 = os.path.join(folder,  'step_3-data/')
    path4 = os.path.join(folder,  'step_4-results/')
    path5 = os.path.join(folder,  'src/')
    path6 = os.path.join(folder, 'step_5-statistics/')

    #hardcoded directories (not using step_0-INPUT folder)
    blocklistsDir = '/home/rezfan/prototype/proto/Data/merged/'
    binfilesDir = '/home/dnsmon/data/'

    #names = IMPORTANT list because will contain names of files that will be used as input for functions
    names = []

    #List all files in directory
    all_files_in_0 = os.listdir(blocklistsDir)
    #List all .txt files in directory
    txt_files = filter(lambda x: x[-4:] == '.txt', all_files_in_0)

    #FIRST STEP: (getting domainlists only)
    #Function takes myFile as input, which is all the entries in the list of txt_files
    def convertMe(myFile):
        #list that will later be appended to file:
        mylist = []

        #Open merged blocklist and create seperate blocklist containing domains only (no providers)
        with open(blocklistsDir+ myFile, "r+") as f:
            for line in f:
                mydomain = line.split("|", 1)
                substring = mydomain[0]
                mylist.append(substring)

        #saving the file to other folder
        newFile = open(path1 + myFile, "w")
        for element in mylist:
            newFile.write(element + '\n')
        newFile.close()

    #executing function convertMe to all txt_files, check length to see if it is new or already adjusted
    for element in txt_files:
        if len(element) == 14:
            convertMe(element)
            print("Filtered to domain_only: "+ element)



#------------------------------------------------------------
#   This section ensures correct txt file names for blocklists
    all_filtered_dbl = os.listdir(path1)
    txt_files2 = filter(lambda x: x[-4:] == '.txt', all_filtered_dbl)

    def txtCorrectName(text_name):
        oldname = text_name
        newname = oldname.replace("-", "")
        names.append(newname[:-4])
        os.rename(path1+  oldname, path1 +  newname)
        os.rename(blocklistsDir + oldname, blocklistsDir+ newname)



    #Changing name for all required files(not the ones that have already been adjusted):
    for element in txt_files2:
        if len(element) == 14:
            txtCorrectName(element)

    print("Given all blocklists and filtered blocklists their correct names")
#-----------------------------------------------------------------------------
#    This section ensures correct bin file names for bloomfilters
    #list allfiles
    all_files_in_2 = os.listdir(binfilesDir)
    bin_files = filter(lambda x: x[-4:] == '.bin', all_files_in_2)

    def binCorrectName(bin_name):
        oldname = bin_name
        def split(word):
            return [char for char in word]

        charlist = split(bin_name)
        newBinName_list = charlist[3:14]
        newBinName = ''.join(map(str, newBinName_list)) + '.bin'

        os.rename(binfilesDir+ oldname, binfilesDir+newBinName)

    for element in bin_files:
        if len(element) == 32:
            binCorrectName(element)
    print("Given .bin files correct names")

    all_files_in_2 = os.listdir(binfilesDir)
    bin_files_updated = filter(lambda x: x[-4:] == '.bin', all_files_in_2)

#----------------------------------------------------------------------------
#Section to return hits:
    def gettingHits2(fileName):
        inputDBL = fileName[:8]
        renamedName = fileName[:-4]
        cmd = path5 + './lookup' + " " + binfilesDir + fileName + " " + path1 + inputDBL + ".txt" + " > " + path2 + renamedName + ".txt"
        os.system(cmd)

    with open(path0+'CompletedLogs/'+'completedBin_adjusted.txt') as file:
        completedBin_adjusted = file.readlines()
        completedBin_adjusted = [line.rstrip() for line in completedBin_adjusted]


    print("Retrieving hits from bloomfilter .bin files..")
    for element in bin_files_updated:
        if element not in completedBin_adjusted:
            gettingHits2(element)
            with open(path0+'CompletedLogs/'+'completedBin_adjusted.txt', 'a') as file:
                file.write(element + '\n')

    print("Hitlist generated")
#-----------------------------------------------------------------------------
    hitlistdir = os.listdir(path2)
    hitlistnames = filter(lambda x: x[-4:] == '.txt', hitlistdir)

#This section covers retrieving original data:
    with open(path0+'CompletedLogs/'+'hitted.txt') as file:
        hitHistory = file.readlines()
        hitHistory = [line.rstrip() for line in hitHistory]


    def getOGData(name):


        mylist = []
        DBLname = name[:-7]
        with open(path2+ name , "r+") as f1, open( blocklistsDir+ DBLname + ".txt", "r+") as f2:
            for hit_line in f1:
                hit_line = hit_line.strip()
                for unf_line in f2:
                    unf_line = unf_line.strip()
                    if hit_line in unf_line:
                        mylist.append(unf_line)
                        break

        newFile = open(path3 + name, "w")
        for element in mylist:
            newFile.write(element + '\n')
        newFile.close()
#------------------------------------------------------------------------------
#This section covers the dataframe generation from which .xlsx documents will be created for the given hours and days of the bloomfilters.
    def dataframeGeneration(input):
        with open(path3 +input, "r+") as f:
            lines = f.read()

            first = lines.split('\n', 1)[0]
            datum = first.split("|")
            colName = datum[-1]

            def Convert(lst):
                res_dct = {lst[i]: 0 for i in range(0,len(lst))}
                return res_dct

            providerList = []
            providerList.append('coinblocker')
            providerList.append('cybercrimetracker')
            providerList.append('dbl')
            providerList.append('digitalside')
            providerList.append('hexxium')
            providerList.append('joewein')
            providerList.append('malcode')
            #providerList.append('malwaredomainlist') #need to be removed
            providerList.append('openphish')
            providerList.append('phishingarmy')
            providerList.append('ponmocup')
            providerList.append('quidsup')
            providerList.append('tolousecrypto')
            providerList.append('tolouseddos')
            providerList.append('tolousemalware')
            providerList.append('tolousephishing')
            providerList.append('urlhaus')
            providerList.append('vxvault')
            providerList.append('technoy')
            providerList.append('rpilist')
            providerList.append('blproject_mal')
            providerList.append('blproject_ran')
            providerList.append('rpilist_crypto' )
            providerList.append('horusteknoloji' )

            dict = Convert(providerList)

            f.seek(0)
            lines2 = f.readlines()
            for line in lines2:
                inList = line.split("|")
                entries = inList[1].split(' ')
                for entry in entries:
                    dict[entry] += 1

            ############ Counters #####################



            providerCounts=[]
            providerCounts = dict.values()


            ##################################################
        data = {
            'Providers':providerList,
            colName:providerCounts
            }

        df = pd.DataFrame(data)

        excelName = input[:-4] + '.xlsx'
        df.to_excel(path4 + excelName)
        df.plot(kind ='bar', x='Providers', y = colName)
        #plt.yscale("symlog")
        plt.yscale("log")
        plt.grid()
        #plt.grid(color='r', linestyle='-', linewidth=2)
        plt.savefig(path6 + input[:-4] + '.png', dpi=300, bbox_inches='tight')
        plt.close('all')


    for element in hitlistnames:
        if element not in hitHistory:
            getOGData(element)
            dataframeGeneration(element)
            with open(path0+'CompletedLogs/'+'hitted.txt', 'a') as file:
                file.write(element + '\n')
    print('Retrieved original data from hitlist')
#------------------------------------------------------------------------------

    print('Exported files to .xlsx format')
#Paths are not hardcoded so the directory in which run.py is needs to be given as argument
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('folder', help='specify the folder you are in', type =str)
    args = parser.parse_args()
    print_paths(args.folder)
    print('Order executed')

if __name__ == "__main__":
    main()
